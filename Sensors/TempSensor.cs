class TemperaturSensor
{
    float actTemp = 20.5f;
    string TSName = "";

    public float GetTemp()
    {
        return actTemp;
    }

    public TemperaturSensor(string SName)
    {
        TSName = SName;
    }
}
