﻿using System;

class MainProg
{
    string ObjName = "";

    static void Main()
    {
        Console.WriteLine("Im Main");
        //var mp = new MainProg("Obj1");
        var temSens1 = new TemperaturSensor("TS Raum 1");
        Console.WriteLine("Aktuelle Temperatur: " + temSens1.GetTemp());
        var temSens2 = new TemperaturSensor("TS Raum 2");
        Console.WriteLine("Aktuelle Temperatur: " + temSens2.GetTemp());
    }

    MainProg(string AObjName)
    {
        ObjName = AObjName;
        Console.WriteLine("Im Constructor " + ObjName);
    }

    void UserInput()
    {
        Console.WriteLine("Geben Sie einen String ein:");
        string str = Console.ReadLine();
        Console.WriteLine("Sie haben: " + str.ToLower() + " Eingegeben.".ToLower());
    }
}
